import 'package:firebase_auth/firebase_auth.dart';

import 'package:advis/modal/user.dart' as Users;


class AuthService {
  final FirebaseAuth _auth = FirebaseAuth.instance;

  Users.User _userFromFirebaseUser(User user) {
    return user != null ? Users.User(userId: user.uid) : null;
  }


  

  Future signInWithEmailAndPassword(String email, String password) async {
    try {
    
     User user =(await _auth .signInWithEmailAndPassword(email:email, password:password)).user;
    
      return _userFromFirebaseUser(user);
    } catch (e) {
      print(e.toString());
      return null;
    }
  }

  Future signUpWithEmailAndPassword(String email, String password) async {
    try {
      User user = (await _auth.createUserWithEmailAndPassword(
          email: email, password: password)).user;
    
      return _userFromFirebaseUser(user);
    } catch (e) {
      print(e.toString());
      return null;
    }
  }

 Future signOut() async {
    try {
      return await _auth.signOut();
    } catch (e) {
      print(e.toString());
      return null;
    }
  }
  
  Future resetPass(String email) async {
    try {
      return await _auth.sendPasswordResetEmail(email: email);
    } catch (e) {
      print(e.toString());
      return null;
    }
  }
}