import 'dart:convert';

import 'package:advis/helper/constants.dart';
import 'package:advis/modal/user.dart';
import 'package:advis/services/env.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:http/http.dart';

class HelperFunctions {
  static String sharedPreferenceUserLoggedInKey = "ISLOGGEDIN";
  static String sharedPreferenceUserNameKey = "USERNAMEKEY";
  static String sharedPreferenceUserEmailKey = "USERMAILKEY";
  

  //guardar preferencia sharedPrefewrence
  static Future<bool> saveuserLoggedInSharedPreference(
      bool isUserLoggedIn) async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    return await prefs.setBool(sharedPreferenceUserLoggedInKey, isUserLoggedIn);
  }

  static Future<bool> saveUserNameSharedPreference(String userName) async {
    Constants.myName = userName;
    SharedPreferences prefs = await SharedPreferences.getInstance();
    return await prefs.setString(sharedPreferenceUserNameKey, userName);
  }

  static Future<bool> saveUserNameEmailSharedPreference(
      String userEmial) async {
    Constants.myEmail = userEmial;
print(userEmial);
 getUserData(userEmial);

    SharedPreferences prefs = await SharedPreferences.getInstance();
    return await prefs.setString(sharedPreferenceUserEmailKey, userEmial);
  }

//Traer informacion

  static Future<bool> getuserLoggedInSharedPreference() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    bool logged = prefs.getBool(sharedPreferenceUserLoggedInKey);
    if (logged == true) {
      Constants.myEmail = await getuserEmailInSharedPreference();
      Constants.myName = await getuserNameInSharedPreference();
      userLogged = User();
     getUserData(Constants.myEmail);
    }
    return prefs.getBool(sharedPreferenceUserLoggedInKey);
  }

  static Future<String> getuserNameInSharedPreference() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    return prefs.getString(sharedPreferenceUserNameKey);
  }

  static Future<String> getuserEmailInSharedPreference() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    return prefs.getString(sharedPreferenceUserEmailKey);
  }


static getUserData(String userEmial) async{
   Response request = await get(server + '/api/getUserData/' + userEmial);
   print(userEmial);
    var data = json.decode(request.body);
    print(
        "Hola mundo aqui divide ------------------------------------------------------");
    parceData(data);
}





  static parceData(var userData) {
       List <Skill>skills = [];

    for (Map skill in userData['skills'])
     {
       print(skill);
    skills.add(Skill(
          id: skill["id"],
          id_skill: skill["id_skill"],
          level_skill: skill["level_skill"],
          name: skill["name"]));
    }
    print (skills[0].name);
    userLogged = User(
      name: userData['name'] ?? "",
      last_name: userData['last_name'] ?? "",
      id: userData['id'] ,
      qualification: userData['qualification'].toString() ?? "",
      profile_image: userData['profile_image'] ?? "",
      phone_number: userData['phone_number'] ?? "",
      is_online: userData['is_online'] ?? 1,
      is_active: userData['is_active'] ?? 3,
      id_role: userData['id_role'] ?? 3,
      email: userData['email'] ?? "",
      description_student: userData['description_student'] ?? "",
      date_of_birth: userData['date_of_birth']?? "",
      advice_given: userData['advice_given'].toString() ?? "",
      typeUser: userData['typeUser'] ?? 3,
      skills: skills

    );

print(userLogged);


  }





}
