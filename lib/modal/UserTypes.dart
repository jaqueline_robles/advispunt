import 'package:google_maps_flutter/google_maps_flutter.dart';

enum UserType { student, teacher }

class Person {
  String id;
  LatLng position;
  String name;
  bool active;
  String email;

  Person({this.id, this.position, this.active, this.name, this.email});
}
