import 'dart:convert';

class Skill {
  int id;
  int id_skill;

  String level_skill;

  String name;

  Skill({this.id, this.id_skill, this.level_skill, this.name});
}

class User {
  String email;
  String name;
  String last_name;
  String phone_number;
  String profile_image;
  int is_active;
  String date_of_birth;
  String description_student;
  int id_role;
  int is_online;
  String advice_given;
  String qualification;
  int id;
  List<Skill> skills;
  int typeUser;

  String userId;
  User(
      {this.userId,
      this.email,
      this.name,
      this.last_name,
      this.phone_number,
      this.profile_image,
      this.is_active,
      this.date_of_birth,
      this.description_student,
      this.id_role,
      this.is_online,
      this.advice_given,
      this.qualification,
      this.id,
      this.typeUser,
     this.skills}); 
}
