import 'package:advis/helper/constants.dart';
import 'package:advis/helper/helperfunctions.dart';
import 'package:advis/modal/skill.dart';
import 'package:advis/modal/user.dart';
import 'package:advis/pages/mapScreen.dart';
import 'package:advis/pages/search.dart';
import 'package:advis/pages/navigation.dart';
import 'package:advis/services/env.dart';
import 'package:flutter/material.dart';
import 'package:http/http.dart';
import 'dart:convert';
import 'package:date_field/date_field.dart';

class EditSkillTeacher extends StatefulWidget {
  final email;

  EditSkillTeacher(this.email);
  @override
  _EditSkillTeacherState createState() => _EditSkillTeacherState();
}

class _EditSkillTeacherState extends State<EditSkillTeacher> {
  String name = "";
  String lastName = "";
  String phone = "";
  String email = "";
  String password = "";
  bool typeUser = true;
  DateTime selectedDate;
  List<Skill> getSkill = [];

   getDatosSkill() async {
    var peticion = await get(server + '/api/view/skill');
    setState(() {

     dynamic datosSkill = json.decode(peticion.body);
     for (Map skill in datosSkill)
     {
       print(skill);
    getSkill.add(Skill(
          id: skill["id"],
        
          name: skill["name"]));
    }
    });

 
  }

  @override
  void initState() {
       getDatosSkill();
    email = widget.email;
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(title: Text('Nueva información')),
        body: Card(
          child: SingleChildScrollView(
              child: Column(
            children: <Widget>[
              ListView.builder(
                  shrinkWrap: true,
                  itemCount: getSkill.length,
                  itemBuilder: (context, index) {
                    return ListTile(
                      onTap:(){
                          
 storeNewUser(getSkill[index].id); 
 Navigator.pop(context);
                      },
                      title: Text(getSkill[index].name),
                    );
                  }),
              Container(
                  height: 50,
                  padding: EdgeInsets.fromLTRB(10, 0, 10, 0),
                  child: RaisedButton(
                    textColor: Colors.white,
                    color: Colors.blue,
                    child: Text('Regresar'),
                    onPressed: () {
                      Navigator.pop(context);
                    },
                  )),
              /*Container(
                  child: Row(
                children: <Widget>[
                  Text('¿No tiene cuenta?'),
                  FlatButton(
                    textColor: Colors.blue,
                    child: Text(
                      'Registrarse',
                      style: TextStyle(fontSize: 20),
                    ),
                    onPressed: () {
                      storeNewUser();
                      //signup screen
                    },
                  )
                ],
                mainAxisAlignment: MainAxisAlignment.center,
              ))*/
            ],
          )),
        ));
  }

  storeNewUser(int id) async {
    Map body = {
    "id_skill": id.toString(),
    "id_teacher_user" : userLogged.id.toString(),

    };

    String datosEnjson = json.encode(body);
    print(datosEnjson);

    Response response =
        await post(server + '/api/add/skill/teacher', body: body);
    print(response.statusCode);

    if (response.statusCode == 200) {
      HelperFunctions.getUserData(Constants.myEmail);
    }
  }
}
