import 'dart:async';
import 'dart:typed_data';
import 'dart:ui';

import 'package:advis/helper/constants.dart';
import 'package:advis/modal/UserTypes.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:location/location.dart';

import 'dart:math' as math;

class MapScreen extends StatefulWidget {
 

  const MapScreen({Key key, }) : super(key: key);

  _MapScreenState createState() => _MapScreenState();
}

class _MapScreenState extends State<MapScreen> {
  GoogleMapController _mapController;
  Location location = Location();
  StreamSubscription<LocationData> subscription;
  StreamSubscription<QuerySnapshot> document;
  List<Person> persons = List();
  Set<Marker> markers = Set();
  UserType userType;
  String userEmail;
  Person userPerson;
  @override
  void initState() {
    print(userLogged.typeUser);

    print(userLogged.id_role);
    super.initState();
    if (userLogged.typeUser == 1 || userLogged.id_role == 1) {
             userType = UserType.teacher;
          } else {
           userType = UserType.student;
          }

    

    userEmail = Constants.myEmail;
    _iniciarUbicacion();
  }

  _iniciarUbicacion() async {
    bool _serviceEnabled;
    PermissionStatus _permissionGranted;

    _serviceEnabled = await location.serviceEnabled();
    if (!_serviceEnabled) {
      _serviceEnabled = await location.requestService();
      if (!_serviceEnabled) {
        return;
      }
    }

    _permissionGranted = await location.hasPermission();
    if (_permissionGranted == PermissionStatus.denied) {
      _permissionGranted = await location.requestPermission();
      if (_permissionGranted != PermissionStatus.granted) {
        return;
      }
    }

    document = FirebaseFirestore.instance
        .collection("users")
        .snapshots()
        .listen((event) {
      persons = event.docs
          .where((person) =>
              person.data()["active"] != null &&
              person.data()["active"] == true)
          .map((person) {
        Person currentPerson = Person(
          id: person.id,
          name: person.data()['name'] ?? "null",
          active: person.data()['active'] ?? false,
          email: person.data()['email'] ?? "null",
          position:
              LatLng(person.data()['latitude'], person.data()['longitude']) ??
                  LatLng(31.6538179, -106.5890206),
        );

        if (person.data()['email'] == userEmail) userPerson = currentPerson;

        return currentPerson;
      }).toList();

      _createMarkers();
    });

    subscription = location.onLocationChanged.listen((LocationData event) {
      if (_mapController != null && persons.length> 0 )  {
        double minX =
            persons.map((e) => e.position.latitude).reduce((math.min));
        double minY =
            persons.map((e) => e.position.longitude).reduce((math.min));
        double maxX =
            persons.map((e) => e.position.latitude).reduce((math.max));
        double maxY =
            persons.map((e) => e.position.longitude).reduce((math.max));
        minX = math.min(minX, event.latitude);
        minY = math.min(minY, event.longitude);
        maxX = math.max(maxX, event.latitude);
        maxY = math.max(maxY, event.longitude);

        LatLngBounds bounds = LatLngBounds(
            southwest: LatLng(minX, minY), northeast: LatLng(maxX, maxY));
        _mapController.animateCamera(CameraUpdate.newLatLngBounds(bounds, 50));
      }

      FirebaseFirestore.instance
          .collection("users")
          .where("email", isEqualTo: userEmail)
          .get()
          .then((value) {


        FirebaseFirestore.instance
            .collection("users")
            .doc(value.docs[0].id)
            .update({"latitude": event.latitude, "longitude": event.longitude});
      });

      
    });
  }

  @override
  void dispose() {
    if (subscription != null) {
      subscription.cancel();
    }
    if (document != null) {
      document.cancel();
    }
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      floatingActionButton: userType == UserType.teacher
          ? Padding( padding:EdgeInsets.only(bottom: 10), child: FloatingActionButton(
              backgroundColor: userPerson != null ?(userPerson.active ? Colors.green : Colors.black): Colors.blue,
              
              onPressed: () async {
                if (userPerson != null && userType == UserType.teacher)
                  userPerson.active = !userPerson.active;
                FirebaseFirestore.instance
                    .collection("users")
                    .where("email", isEqualTo: userEmail)
                    .get()
                    .then((value) {
                  FirebaseFirestore.instance
                      .collection("users")
                      .doc(value.docs[0].id)
                      .update({
                    "active": userPerson == null ? true : userPerson.active
                  });
                });
                setState(() {});
              },
              child: userPerson != null
                  ? userPerson.active
                      ? Icon(Icons.done)
                      : Icon(Icons.cancel)
                  : Icon(Icons.done),
            ))
          : null,
          floatingActionButtonLocation: FloatingActionButtonLocation.startDocked,
      body: GoogleMap(
        initialCameraPosition:
            CameraPosition(target: LatLng(31.6538179, -106.5890206), zoom: 16),
        zoomGesturesEnabled: true,
        myLocationEnabled: true,
        myLocationButtonEnabled: true,
        markers: markers,
        onMapCreated: (controller) => _mapController = controller,
      ),
    );
  }

  void _createMarkers() async {
    Set<Marker> newMarkers = Set();

    await Future.forEach(persons, (person) async {
      var bitmapData;
      if (person.email == userEmail)
        bitmapData = await _createAvatar(100, 100, "YO", color: Colors.black);
      else
        bitmapData = await _createAvatar(
            100, 100, person.name[0].toString().toUpperCase());

      var bitmapDescriptor = BitmapDescriptor.fromBytes(bitmapData);
      var marker = Marker(
          markerId: MarkerId(person.id),
          position: person.position,
          icon: bitmapDescriptor);
      newMarkers.add(marker);
    });

    setState(() {
      markers = newMarkers;
    });
  }

  Future<Uint8List> _createAvatar(int width, int height, String name,
      {Color color = Colors.blue}) async {
    final PictureRecorder pictureRecorder = PictureRecorder();
    final Canvas canvas = Canvas(pictureRecorder);
    final Paint paint = Paint()..color = color;

    canvas.drawOval(
      Rect.fromCircle(
          center: Offset(width * 0.5, height * 0.5),
          radius: math.min(width * 0.5, height * 0.5)),
      paint,
    );

    TextPainter painter = TextPainter(textDirection: TextDirection.ltr);

    painter.text = TextSpan(
      text: name,
      style: TextStyle(fontSize: 50.0, color: Colors.white),
    );

    painter.layout();
    painter.paint(
      canvas,
      Offset((width * 0.5) - painter.width * 0.5,
          (height * 0.5) - painter.height * 0.5),
    );

    final img = await pictureRecorder.endRecording().toImage(width, height);
    final data = await img.toByteData(format: ImageByteFormat.png);

    return data.buffer.asUint8List();
  }
}
