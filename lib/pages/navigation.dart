//en esta vista se pretende que se muestre despues de que se incio seccion en el dispositivo
//se visualiza las materias que tienen disponibles asesores para las asesorias

import 'package:advis/helper/constants.dart';
import 'package:advis/pages/chatRoomsScreen.dart';
import 'package:advis/pages/mapScreen.dart';
import 'package:advis/pages/notification.dart';

import 'package:advis/pages/perfil_user_estudent.dart';
import 'package:advis/pages/profilePrivateTeacher.dart';
import 'package:advis/pages/profile_user_teacher.dart';
import 'package:advis/pages/types_of_consultancies.dart';
import 'package:advis/services/env.dart';
import 'package:flutter/material.dart';
import 'package:curved_navigation_bar/curved_navigation_bar.dart';
import 'package:http/http.dart';
import 'dart:convert';

class AdvisNavigation extends StatefulWidget {
  @override
  _AdvisNavigationState createState() => _AdvisNavigationState();
}

class _AdvisNavigationState extends State<AdvisNavigation> {
  int pageIndex = 1;

  //aqui me traigo las otras vistas para poder navegar entre ellas
  final AdvisNavigation _listAdvisNavigation = AdvisNavigation();

final ConsultanciesView _consultanciesView = ConsultanciesView(); 

  final MapScreen _mapsView = MapScreen();
  final ChatRoom _chatRooms = ChatRoom();
  final ProfileApp _profileUser = ProfileApp();
  final NotificationView _notificationView = NotificationView();
  final ProfileTeacher _profileTeacher = ProfileTeacher();
  final ProfilePrivateTeacher _privateProfileTeacher = ProfilePrivateTeacher();

//aqui indico cual es la pagina primera que se tiene que abrir al iniciar

  Widget _showPage = MapScreen();

  Widget _pageChooser(int page) {
    switch (page) {
      case 0:
        {
          if (userLogged.typeUser == 1 || userLogged.id_role == 1) {
            return _notificationView;
          } else {
            return _consultanciesView; 
          }
        }
        break;

      case 1:
        return _mapsView;
        break;

      case 2:
        return _chatRooms;
        break;

      case 3:
        if (userLogged.typeUser == 1 || userLogged.id_role == 1) {
          return _privateProfileTeacher;
        } else {
          return _profileUser;
        }
        break;

      default:
        return new Container(
          child: new Center(
            child: new Text('La pagina no esta funcional'),
          ),
        );
    }
  }

  @override
  Widget build(BuildContext context) {
    var curvedNavigationBar = CurvedNavigationBar(
      index: pageIndex,
      height: 50.0,
      items: <Widget>[
        Icon(Icons.list, size: 30),
        Icon(Icons.gps_fixed, size: 30),
        Icon(Icons.message, size: 30),
        Icon(Icons.perm_identity, size: 30),
      ],
      color: Colors.white,
      buttonBackgroundColor: Colors.white,
      backgroundColor: Colors.blueAccent,
      animationCurve: Curves.easeInOut,
      animationDuration: Duration(milliseconds: 600),
      onTap: (int tappedIndex) {
        setState(() {
          print(tappedIndex);
          pageIndex = tappedIndex;
          _showPage = _pageChooser(tappedIndex);
        });
      },
    );
    return Scaffold(
      backgroundColor: Colors.blueAccent,
      bottomNavigationBar: curvedNavigationBar,
      appBar: AppBar(
        centerTitle: true,
        title: Text("Asesorías"),
        actions: <Widget>[
          IconButton(
              icon: Icon(Icons.search),
              onPressed: () {
                showSearch(context: context, delegate: DataSearch());
              })
        ],
      ),
      body: Container(
        color: Colors.grey,
        child: Center(
          child: _showPage,
        ),
      ),
    );
//drawer: Drawer(),
  }
} //es del final de la clase despues del scafol

//traer dotos de esquill y mostrarlos start
