import 'package:advis/pages/mapScreen.dart';
import 'package:advis/pages/search.dart';
import 'package:advis/pages/navigation.dart';
import 'package:advis/services/env.dart';
import 'package:flutter/material.dart';
import 'package:http/http.dart';
import 'dart:convert';
import 'package:date_field/date_field.dart';


class RegisterUser extends StatefulWidget {

  final email;
  

    RegisterUser(this.email);
  @override
  _RegisterUserState createState() => _RegisterUserState();
}

class _RegisterUserState extends State<RegisterUser> {
  String name = "";
  String lastName = "";
  String phone = "";
  String email = "";
  String password = "";
  bool typeUser = true;
  DateTime selectedDate;
  

@override
void initState() {
    email = widget.email;
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(title: Text('Completar información')),
        body: Card(
          child: SingleChildScrollView(
              child: Column(
            children: <Widget>[
              Center(
                child: Container(
                    padding: EdgeInsets.all(10),
                    child: Text(
                      'Casi listo para empezar a usar la plataforma',
                      style: TextStyle(fontSize: 20),
                    )),
              ),
              Container(
                padding: EdgeInsets.all(30),
                child: TextField(
                  onChanged: (texto) {
                    name = texto;
                  },
                  decoration: InputDecoration(
                      border: OutlineInputBorder(),
                      labelText: 'Nombre',
                      hintText: 'Ingresa tu nombre'),
                ),
              ),
              Container(
                padding: EdgeInsets.all(30),
                child: TextField(
                  onChanged: (texto) {
                    lastName = texto;
                  },
                  decoration: InputDecoration(
                      border: OutlineInputBorder(),
                      labelText: 'Apellido',
                      hintText: 'Ingresa tu Apellido'),
                ),
              ),
              Container(
                padding: EdgeInsets.all(30),
                child: TextField(
                  onChanged: (texto) {
                    phone = texto;
                  },
                  decoration: InputDecoration(
                      border: OutlineInputBorder(),
                      labelText: 'Numero telefónico',
                      hintText: 'Ingresa tu Apellido'),
                ),
              ),
              /*Container(
                padding: EdgeInsets.all(30),
                child: TextField(
                  onChanged: (texto) {
                    email = texto;
                  },
                  decoration: InputDecoration(
                      border: OutlineInputBorder(),
                      labelText: 'Correo electronico',
                      hintText: 'Ingresa tu correo electronico activo'),
                ),
              ),
              Container(
                padding: EdgeInsets.all(30),
                child: TextField(
                  onChanged: (texto) {
                    password = texto;
                  },
                  obscureText: true,
                  decoration: InputDecoration(
                      border: OutlineInputBorder(),
                      labelText: 'ingresa una contraseña porfavor',
                      hintText: 'Contraseña'),
                ),
              ),*/
              Container(
child: DateTimeField(
  mode: DateFieldPickerMode.date,
                selectedDate: selectedDate,
                onDateSelected: (DateTime date) {
                  setState(() {
                    selectedDate = date;
                  });
                },
                lastDate: DateTime(2020),
              ),
              ),
              Container(
                  padding: EdgeInsets.all(30),
                  child: Row(
                    children: <Widget>[
                      Text('Maestro'),
                      Switch(
                        value: typeUser,
                        onChanged: (booleano) {
                          setState(() {
                            typeUser = booleano;
                          });
                         
                        },
                      ),
                      Text('Alumno')
                    ],
                  )),
              FlatButton(
                onPressed: () {
                  //forgot password screen
                },
                textColor: Colors.blue,
                child: Text(''),
              ),
              Container(
                  height: 50,
                  padding: EdgeInsets.fromLTRB(10, 0, 10, 0),
                  child: RaisedButton(
                    textColor: Colors.white,
                    color: Colors.blue,
                    child: Text('Siguiente'),
                    onPressed: () {
                      storeNewUser();
                      Navigator.push(
              context, MaterialPageRoute(builder: (context) => AdvisNavigation()));
                    },
                  )),
              /*Container(
                  child: Row(
                children: <Widget>[
                  Text('¿No tiene cuenta?'),
                  FlatButton(
                    textColor: Colors.blue,
                    child: Text(
                      'Registrarse',
                      style: TextStyle(fontSize: 20),
                    ),
                    onPressed: () {
                      storeNewUser();
                      //signup screen
                    },
                  )
                ],
                mainAxisAlignment: MainAxisAlignment.center,
              ))*/
            ],
          )),
        ));
  }

  storeNewUser() async {
    Map body = {
      "name": name,
      "last_name": lastName,
      "phone_number": phone,
      "email": email,
      "password": password,
      "date_of_birth": selectedDate.toString()
    };

    String datosEnjson = json.encode(body);
print(datosEnjson);
    if (typeUser == true)
    {

Response response =
        await post(server + '/api/add/studentNew', body:body );
    print(response.statusCode);
    } 
    else
    {
     
    Response response =
        await post(server + '/api/add/teacherNew', body: body);
    print(response.statusCode);
    }

  }
}
