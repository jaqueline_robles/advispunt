import 'dart:convert';

import 'package:advis/helper/constants.dart';
import 'package:advis/helper/helperfunctions.dart';
import 'package:advis/modal/user.dart';
import 'package:advis/pages/chatRoomsScreen.dart';
import 'package:advis/pages/editProfileTeacher.dart';
import 'package:advis/pages/editSkillTeacher.dart';
import 'package:advis/services/database.dart';
import 'package:advis/services/env.dart';
import 'package:flutter/material.dart';
import 'package:http/http.dart';

class ProfilePublicTeacher extends StatefulWidget {
String teacherId;
String email;
String typeUser;
String nameSkill;
ProfilePublicTeacher(this.teacherId,  this.typeUser, this.email, this.nameSkill);




  @override
  _ProfilePublicTeacherState createState() => _ProfilePublicTeacherState();
}

class _ProfilePublicTeacherState extends State<ProfilePublicTeacher> {

User teacher = User(
      name: "",
      last_name: "",
      id: 0,
      qualification:  "",
      profile_image:  "",
      phone_number:  "",
      is_online: 1,
      is_active:  3,
      id_role:  3,
      email:  "",
      description_student:  "",
      date_of_birth:  "",
      advice_given:  "",
      typeUser:  3,
      skills: [],

    );



getUserData(String userEmial) async{
   Response request = await get(server + '/api/getUserData/' + userEmial);
   print(userEmial);
    var data = json.decode(request.body);
    print(
        "Hola mundo aqui divide ------------------------------------------------------");
    parceData(data);
}





  parceData(var userData) {
       List <Skill>skills = [];

    for (Map skill in userData['skills'])
     {
       print(skill);
    skills.add(Skill(
          id: skill["id"],
          id_skill: skill["id_skill"],
          level_skill: skill["level_skill"],
          name: skill["name"]));
    }
    print (skills[0].name);
    HelperFunctions.getUserData(Constants.myEmail);
    teacher = User(
      name: userData['name'] ?? "",
      last_name: userData['last_name'] ?? "",
      id: userData['id'] ,
      qualification: userData['qualification'] ?? "",
      profile_image: userData['profile_image'] ?? "",
      phone_number: userData['phone_number'] ?? "",
      is_online: userData['is_online'] ?? 1,
      is_active: userData['is_active'] ?? 3,
      id_role: userData['id_role'] ?? 3,
      email: userData['email'] ?? "",
      description_student: userData['description_student'] ?? "",
      date_of_birth: userData['date_of_birth']?? "",
      advice_given: userData['advice_given']?? "",
      typeUser: userData['typeUser'] ?? 3,
      skills: skills

    );
    print(Constants.myEmail);
setState(() {

  
});
print(teacher);


  }

 @override
  void initState() {
    print("--------------------------gggggggggggggggggggggggggggggggggggggg----------------------------");
    print(widget.email);
   getUserData(widget.email);


    super.initState();
  }
showAlertDialog(BuildContext context) {

  // set up the buttons
  Widget remindButton = FlatButton(
    child: Text("Cancelar"),
    onPressed:  () {

      Navigator.pop(context);
    },
  );
  Widget cancelButton = FlatButton(
    child: Text("Remota"),
    onPressed:  () {
      print("cualquier cosa------------------------------------------------");

      requestRemoteChat(1);
    },
  );
  Widget launchButton = FlatButton(
    child: Text("Presencial"),
    onPressed:  () {
      requestRemoteChat(2);
    },
  );

  // set up the AlertDialog
  AlertDialog alert = AlertDialog(
    title: Text("Confirmación"),
    content: Text("Seguro que deceas solicitar una asesoría?"),
    actions: [
      remindButton,
      cancelButton,
      launchButton,
    ],
  );

  // show the dialog
  showDialog(
    context: context,
    builder: (BuildContext context) {
      return alert;
    },
  );
}


requestRemoteChat(type) {

  
print(teacher.id.toString());
print( widget.nameSkill);
print(userLogged.name);
print(userLogged.id);
      Map<String, dynamic> chatRoomMap = {
        "id_teacher": teacher.id.toString(),
        "subject": "Nueva Asesoría disponible de:"+ widget.nameSkill,
        "message": "De parte de:" + userLogged.name,
        "id_student": userLogged.id.toString(),
        "email_student": userLogged.email.toString(),
        "email_teacher": teacher.email.toString(),
        "type" : type
        
      };
      print("imprimeir chatt rommmm -------------------------");

      print(chatRoomMap);

      DatabaseMethods().notificationChatRemote(chatRoomMap);
      Navigator.pop(context);
      // Navigator.push(context,
      //     MaterialPageRoute(builder: (context) => ChatRoom(
            
      //     )));
    } 



  @override
  Widget build(BuildContext context) {
    return Scaffold(
              appBar: AppBar(title: Text('Perfil')),
      body: SingleChildScrollView(
        child: Column(
          children: <Widget>[
            Container(
                decoration: BoxDecoration(
                    gradient: LinearGradient(
                        begin: Alignment.topCenter,
                        end: Alignment.bottomCenter,
                        colors: [Colors.blue[300], Colors.blue[200]])),
                child: Container(
                  width: double.infinity,
                  height: 350.0,
                  child: Center(
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.center,
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: <Widget>[
                        CircleAvatar(
                          backgroundImage: NetworkImage(
                           server + teacher.profile_image,
                          ),
                          radius: 50.0,
                        ),
                        SizedBox(
                          height: 10.0,
                        ),
                        Text(
                          teacher.name + " " + teacher.last_name,
                          style: TextStyle(
                            fontSize: 22.0,
                            color: Colors.white,
                          ),
                        ),
                        SizedBox(
                          height: 10.0,
                        ),
                
                      ],
                    ),
                  ),
                )),


Container(
  padding:  EdgeInsets.all(10),
  child:
  RaisedButton(
                  onPressed: () {

                     showAlertDialog(context);
                  },
                  shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(80.0)),
                  elevation: 0.0,
                  padding: EdgeInsets.all(0.0),
                  child: Ink(
                  
                    decoration: BoxDecoration(
                      gradient: LinearGradient(
                          begin: Alignment.centerRight,
                          end: Alignment.centerLeft,
                          colors: [Colors.blueAccent, Colors.blueGrey]),
                      borderRadius: BorderRadius.circular(30.0),
                    ),
                    child: Container(
                      constraints:
                          BoxConstraints(maxWidth: 300.0, minHeight: 50.0),
                      alignment: Alignment.center,
                      child: Text(
                        "Solicitar asesoría",
                        style: TextStyle(
                            color: Colors.white,
                            fontSize: 26.0,
                            fontWeight: FontWeight.w300),
                      ),
                    ),
                  )),
),



            Container(

              
              child: Padding(
                padding: const EdgeInsets.symmetric(
                    vertical: 30.0, horizontal: 16.0),
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                    Text(
                      "Habilidades:",
                      style: TextStyle(
                          color: Colors.blue,
                          fontStyle: FontStyle.normal,
                          fontSize: 28.0),
                    ),
                    SizedBox(
                      height: 10.0,
                    ),
                   
ListView.builder(
  shrinkWrap: true,
  itemCount: teacher.skills.length,
  itemBuilder:(context , index){
    return ListTile(
      title:  Text(teacher.skills[index].name),

    );
  }
)


                  ],
                ),
              ),
            ),
            SizedBox(
              height: 20.0,
            ),
           
            Padding(padding: EdgeInsets.only(bottom: 50))
          ],
        ),
      ),
    );
  }
}
