// import 'dart:async';

// import 'package:GeoMeet/models/UserTypes.dart';
// import 'package:cloud_firestore/cloud_firestore.dart';
// import 'package:flutter/material.dart';
// import 'package:flutter_polyline_points/flutter_polyline_points.dart';
// import 'package:google_maps_flutter/google_maps_flutter.dart';
// import 'package:location/location.dart';

// class RouteMapScreen extends StatefulWidget {
//   final LatLng destination;
//   final LatLng from;

//   RouteMapScreen(
//       {this.from = const LatLng(31.6538179, -106.5890206),
//       @required this.destination});
//   @override
//   _RouteMapScreenState createState() => _RouteMapScreenState();
// }

// class _RouteMapScreenState extends State<RouteMapScreen> {
//   GoogleMapController _mapController;
//   Location location = Location();
//   StreamSubscription<LocationData> subscription;
//   StreamSubscription<QuerySnapshot> document;
//   Set<Marker> markers = Set<Marker>();
//   String apiKey = "AIzaSyCtkkP9lOFjIEfQ_Xo8veycplUdL0O_m9c";
//   Map<PolylineId, Polyline> polylines = {};
//   List<LatLng> polylineCoordinates = [];
//   PolylinePoints polylinePoints = PolylinePoints();

//   void initState() {
//     super.initState();
//     _iniciarUbicacion();
//   }

//   _iniciarUbicacion() async {
//     bool _serviceEnabled;
//     PermissionStatus _permissionGranted;

//     _serviceEnabled = await location.serviceEnabled();
//     if (!_serviceEnabled) {
//       _serviceEnabled = await location.requestService();
//       if (!_serviceEnabled) {
//         return;
//       }
//     }

//     _permissionGranted = await location.hasPermission();
//     if (_permissionGranted == PermissionStatus.denied) {
//       _permissionGranted = await location.requestPermission();
//       if (_permissionGranted != PermissionStatus.granted) {
//         return;
//       }
//     }
//     bool first = false;
//     subscription = location.onLocationChanged.listen((LocationData event) {
//       if (first == false) {
//         _createMarkers(LatLng(event.latitude, event.longitude));
//         setState(() {});
//       }

//       first = true;
//     });
//   }

//   @override
//   Widget build(BuildContext context) {
//     return GoogleMap(
//       initialCameraPosition: CameraPosition(
//         target: widget.from,
//       ),
//       zoomGesturesEnabled: false,
//       myLocationEnabled: true,
//       myLocationButtonEnabled: true,
//       markers: markers,
//       polylines: Set<Polyline>.of(polylines.values),
//     );
//   }

//   _createMarkers(LatLng from) {
//     var tmp = Set<Marker>();

//     tmp.add(Marker(markerId: MarkerId("from"), position: from ?? widget.from));
//     tmp.add(Marker(markerId: MarkerId("to"), position: widget.destination));
//     _getPolyline(from);
//     markers = tmp;
//   }

//   _getPolyline(LatLng from) async {
//     PolylineResult result = await polylinePoints.getRouteBetweenCoordinates(
//       apiKey,
//       PointLatLng(from.latitude, from.longitude),
//       PointLatLng(widget.destination.latitude, widget.destination.longitude),
//       travelMode: TravelMode.transit,
//     );

//     if (result.points.isNotEmpty) {
//       result.points.forEach((PointLatLng point) {
//         print(point);
//         polylineCoordinates.add(LatLng(point.latitude, point.longitude));
//       });
//     }
//     _addPolyLine();
//   }

//   _addPolyLine() {
//     polylines = {};
//     PolylineId id = PolylineId("poly");
//     Polyline polyline = Polyline(
//         polylineId: id, color: Colors.blue, points: polylineCoordinates);
//     polylines[id] = polyline;
//     print(polylines);
//     setState(() {});
//   }
// }
