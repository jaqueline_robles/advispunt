import 'dart:convert';

import 'package:advis/helper/authenticate.dart';
import 'package:advis/helper/constants.dart';
import 'package:advis/modal/skill.dart';
import 'package:advis/services/auth.dart';
import 'package:advis/services/env.dart';
import 'package:flutter/material.dart';
import 'package:http/http.dart';


class ProfileApp extends StatefulWidget {
  @override
  _ProfileAppState createState() => _ProfileAppState();
}

class _ProfileAppState extends State<ProfileApp> {
  bool typeUser = true;
   AuthService authService = new AuthService();

    var datosSkill = [];
    List <Skills> _listSkill = List<Skills>();

  getDatosSkill() async {
    var peticion = await get(server + '/api/view/skill');
    setState(() {
      datosSkill = json.decode(peticion.body);
    });
parceData(datosSkill);
    print(datosSkill);
  }

  @override
  void initState() {
    getDatosSkill();
    super.initState();
  }


  @override
  Widget build(BuildContext context) {
    return Scaffold(
       appBar: AppBar(title: Text('CHAT'), actions: [
        GestureDetector(
          onTap: () {
            authService.signOut();
            Navigator.pushReplacement(context,
                MaterialPageRoute(builder: (context) => Authenticate()));
          },
          child: Container(
              padding: EdgeInsets.symmetric(horizontal: 16),
              child: Icon(Icons.exit_to_app)),
        )
      ]),

      body: SingleChildScrollView(
              child: Column(
          
          children: <Widget>[
            Container(
              decoration: BoxDecoration(
                gradient: LinearGradient(
                  begin: Alignment.topCenter,
                  end: Alignment.bottomCenter,
                  colors: [Colors.blue[300], Colors.blue[200]]
                )
              ),
              child: Container(
                width: double.infinity,
                height: 350.0,
                child: Center(
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.center,
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: <Widget>[
                      CircleAvatar(
                        backgroundImage: NetworkImage(
                          "https://www.rd.com/wp-content/uploads/2017/09/01-shutterstock_476340928-Irina-Bg.jpg",
                        ),
                        radius: 50.0,
                      ),
                      SizedBox(
                        height: 10.0,
                      ),
                      Text(
                        "Alice James",
                        style: TextStyle(
                          fontSize: 22.0,
                          color: Colors.white,
                        ),
                      ),
                      SizedBox(
                        height: 10.0,
                      ),
                      Card(
                        margin: EdgeInsets.symmetric(horizontal: 20.0,vertical: 5.0),
                        clipBehavior: Clip.antiAlias,
                        color: Colors.white,
                        elevation: 5.0,
                        child: Padding(
                          padding: const EdgeInsets.symmetric(horizontal: 8.0,vertical: 22.0),
                          child: Container(
                  padding: EdgeInsets.all(30),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,

                    children: [
                      IconButton(icon:  Icon(Icons.report ,color: Colors.red, size: 50.0, ), onPressed: null),
                      Row(
                        
                        children: <Widget>[
                          Text('Maestro'),
                          Switch(
                            value: typeUser,
                            onChanged: (booleano) {

                              setState(() {
                                
                                typeUser = booleano;
                                storeNewUser();
                              });
                             
                            },
                          ),
                          Text('Alumno')
                        ],
                      ),
                    ],
                  )),
                        ),
                      )
                    ],
                  ),
                ),
              )
            ),
            Container(
              child: Padding(
                padding: const EdgeInsets.symmetric(vertical: 30.0,horizontal: 16.0),
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                
                    Text(
                        "Descripción:",
                      style: TextStyle(
                        color: Colors.blue,
                        fontStyle: FontStyle.normal,
                        fontSize: 28.0
                      ),
                    ),
                    SizedBox(
                      height: 10.0,
                    ),
                    Text('Esta es una prueba de la informacion que es posible agregar',
                      style: TextStyle(
                        fontSize: 22.0,
                        fontStyle: FontStyle.italic,
                        fontWeight: FontWeight.w300,
                        color: Colors.black,
                        letterSpacing: 2.0,
                      ),
                    ),
                  ],
                ),
              ),
            ),
            SizedBox(
              height: 20.0,
            ),
          
            Padding(padding: EdgeInsets.only(bottom: 50))
          ],
        ),
      ),
    );
  }



    storeNewUser() async {
    Map body = {
     
      "email": Constants.myEmail,
      "role": (typeUser == true? 1 : 0).toString(),
      
    };

    String datosEnjson = json.encode(body);
print(datosEnjson);
    if (typeUser == true)
    {

Response response =
        await post(server + '/api/typeUserStudent', body:body );
    print(response.statusCode);
    } 
    else
    {
     
    Response response =
        await post(server + '/api/typeUserStudent', body: body);
    print(response.statusCode);
    }

  }



   parceData(var skillData) {

    for (var skill in skillData) {
      
      _listSkill.add(
         Skills(
      name: skill['name'] ?? "",
      is_active: skill['is_active'] ?? "",
      id: skill['id'] ,
      
  
    )
      );
    }
    
  }


}