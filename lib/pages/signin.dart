import 'package:advis/Widgets/widget.dart';
import 'package:advis/helper/helperfunctions.dart';
import 'package:advis/pages/chatRoomsScreen.dart';
import 'package:advis/pages/mapScreen.dart';
import 'package:advis/pages/navigation.dart';
import 'package:advis/services/auth.dart';
import 'package:advis/services/database.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';

class SignIn extends StatefulWidget {
  final Function toogle;
  SignIn(this.toogle);
  @override
  _SignInState createState() => _SignInState();
}

class _SignInState extends State<SignIn> {
  final formKey = GlobalKey<FormState>();
  AuthService authService = new AuthService();
  DatabaseMethods databaseMethods = new DatabaseMethods();

  TextEditingController emailNameTextEdittingController =
      new TextEditingController();
  TextEditingController passwordNameTextEdittingController =
      new TextEditingController();

  bool isLoading = false;
  QuerySnapshot snapshotUserInfo;
  signIn() {
    if (formKey.currentState.validate()) {
      // HelperFunctions().saveUserNameSharedPreference(passwordNameTextEdittingController.text);
      HelperFunctions.saveUserNameEmailSharedPreference(
          emailNameTextEdittingController.text);
      databaseMethods
          .getUserByUserEmail(emailNameTextEdittingController.text)
          .then((val) {
        snapshotUserInfo = val;
        HelperFunctions.saveUserNameSharedPreference(
            snapshotUserInfo.docs[0].data()["name"]);
        // print("${snapshotUserInfo.docs[0].data()["name"]} esto no es bueno");
      });

      //    Funcion get userDetails
      setState(() {
        isLoading = true;
      });

      authService
          .signInWithEmailAndPassword(emailNameTextEdittingController.text,
              passwordNameTextEdittingController.text)
          .then((val) {
        if (val != null) {
          HelperFunctions.saveuserLoggedInSharedPreference(true);
          Navigator.pushReplacement(
              context, MaterialPageRoute(builder: (context) => AdvisNavigation()));
        }
      });
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: appBarMain(context),
      body: SingleChildScrollView(
        padding: EdgeInsets.symmetric(horizontal: 24),
        child: Column(
          children: [
            Container(
                width: 200,
                height: 200,
                margin: EdgeInsets.only(top: 50.0),
                child:
                Image.asset('assets/img/logo.png')
               ),
            /*esta parte son los capos que son para usuario 
              y contraseña hedera los widgets del archivo widget.dart que pasa
              como parametros el nombre y seencarga de la decoracion*/
            Form(
              key: formKey,
              child: Column(children: [
                TextFormField(
                  validator: (val) {
                    return RegExp(
                                r"^[a-zA-Z0-9.a-zA-Z0-9.!#$%&'*+-/=?^_`{|}~]+@[a-zA-Z0-9]+\.[a-zA-Z]+")
                            .hasMatch(val)
                        ? null
                        : "Ingresar un correo valido";
                  },
                  controller: emailNameTextEdittingController,
                  style: simpleTextStyle(),
                  decoration: textFieldInputDecoration('Correo electrónico'),
                ),
                TextFormField(
                  obscureText: true,
                  validator: (val) {
                    return val.length > 6
                        ? null
                        : "Porfavor ingresar una contraseña valida arriba de 6 cacarteres";
                  },
                  controller: passwordNameTextEdittingController,
                  style: simpleTextStyle(),
                  decoration: textFieldInputDecoration('Contraseña'),
                ),
              ]),
            ),
            /*Esta parte es para dar click si se olvido la contraseña*/
            SizedBox(
              height: 8,
            ),
            Container(
              alignment: Alignment.centerRight,
              child: Container(
                  padding: EdgeInsets.symmetric(horizontal: 16, vertical: 8),
                  child: Text(
                    "¿Olvidaste tu contraseña?",
                    style: simpleTextStyle(),
                  )),
            ),

//Aqui termina el diseño y funcionalidas del olvidaste contraseña
            SizedBox(
              height: 16,
            ),
            /*este contenedor se creo con el fin de crear el boton de iniciar sesion*/
            GestureDetector(
              onTap: () {
                signIn();
              },
              child: Container(
                alignment: Alignment.center,
                width: MediaQuery.of(context).size.width,
                padding: EdgeInsets.symmetric(vertical: 20),
                decoration: BoxDecoration(
                    gradient: LinearGradient(colors: [
                      const Color(0xff007EF4),
                      const Color(0xff2A75BC)
                    ]),
                    borderRadius: BorderRadius.circular(30)),
                child: Text(
                  "Iniciar sesión",
                  style: mediumTextStyle(),
                ),
              ),
            ),
            /*Este contenedor fue creado con el proposito de poner el boton de iniciar session con google*/
            SizedBox(
              height: 16,
            ),
            Container(
              alignment: Alignment.center,
              width: MediaQuery.of(context).size.width,
              padding: EdgeInsets.symmetric(vertical: 20),
              decoration: BoxDecoration(
                  color: Colors.grey, borderRadius: BorderRadius.circular(30)),
              child: Text(
                "Iniciar sesión con Google",
                style: TextStyle(color: Colors.black87, fontSize: 17),
              ),
            ),

/*aqui se realizo la leyenda que indica que si no tienes contraseña te puedes registrar*/
            SizedBox(
              height: 16,
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                Text(
                  '¿No tienes una cuenta?',
                  style: mediumTextStyle(),
                ),
                GestureDetector(
                  onTap: () {
                    widget.toogle();
                  },
                  child: Text('Registrate Ahora',
                      style: TextStyle(
                          color: Colors.black87,
                          fontSize: 17,
                          decoration: TextDecoration.underline)),
                ),
              ],
            ),
          ],
        ),
      ),
    );
  }
}
