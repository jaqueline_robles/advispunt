import 'package:advis/helper/constants.dart';
import 'package:advis/pages/conversation_screen.dart';
import 'package:advis/services/database.dart';
import 'package:flutter/material.dart';
import 'package:cloud_firestore/cloud_firestore.dart';

class NotificationView extends StatefulWidget {
  @override
  _NotificationViewState createState() => _NotificationViewState();
}




class _NotificationViewState extends State<NotificationView> {

  createChatroomAndStartConversation({String userName}) {
    print(userName);
    print("---");
    print("${Constants.myName}");
    if (userName != Constants.myName) {
      String chatRoomId = getChatRoomId(userName, Constants.myName);

      List<String> users = [userName, Constants.myName];
      Map<String, dynamic> chatRoomMap = {
        "users": users,
        "chatroomId": chatRoomId
      };

      DatabaseMethods().createChatRoom(chatRoomId, chatRoomMap);
      Navigator.push(context,
          MaterialPageRoute(builder: (context) => ConversationScreen(
            chatRoomId
          )));
    } else {
      print("No puedes enviarte un mensage a ti mismo");
    }
  }


  





  DatabaseMethods databaseMethods = new DatabaseMethods();
  List<Map> _list = List<Map>();
  Stream chatMessageStream;
  Widget NotificationList() {
    return StreamBuilder(
      stream: chatMessageStream,
      builder: (context, snapshot) {
        return snapshot.hasData
            ? ListView.builder(
                itemCount: snapshot.data.documents.length,
                itemBuilder: (context, index) {
                  return ListTile(
                    title:
                    
                        Text("Tipo" + (snapshot.data.documents[index].data()["type"].toString() == "1" ? "Asesoría remota" : "Asesoría Presencial") + snapshot.data.documents[index].data()["subject"]),
                    subtitle:
                        Text(snapshot.data.documents[index].data()["message"]),
                        trailing: RaisedButton(onPressed: () {

                          if(snapshot.data.documents[index].data()["type"].toString() == "1")
                          {
                            createChatroomAndStartConversation(userName: snapshot.data.documents[index].data()["email_student"].toString());

                          }else{



                          }
                          
                        },
                        child: Text('Aceptar'),
                        
                        ),
                  );
                })
            : Container();
      },
    );
  }

  void initState() {
    databaseMethods.getNotification().then((value) {
      setState(() {
        chatMessageStream = value;
      });
    });
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(child: NotificationList()),
    );
  }
}

getChatRoomId(String a, String b) {
  print(a);
  print(b);
  if (a.substring(0, 1).codeUnitAt(0) > b.substring(0, 1).codeUnitAt(0)) {
    return "$b\_$a";
  } else {
    return "$a\_$b";
  }
}


