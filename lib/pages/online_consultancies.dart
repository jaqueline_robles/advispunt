 import 'package:curved_navigation_bar/curved_navigation_bar.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';

 class OnlineConsultorie extends StatefulWidget {
   @override
   _OnlineConsultorieState createState() => _OnlineConsultorieState();
 }
 
 class _OnlineConsultorieState extends State<OnlineConsultorie> {
    int _page = 0;
  GlobalKey _bottomNavigationKey = GlobalKey();
   @override
   Widget build(BuildContext context) {
     return Scaffold(
     
      bottomNavigationBar: CurvedNavigationBar(
          key: _bottomNavigationKey,
          index: 0,
          height: 50.0,
          items: <Widget>[
            Icon(Icons.list, size: 30),
            Icon(Icons.add, size: 30),
            Icon(Icons.compare_arrows, size: 30),
            Icon(Icons.call_split, size: 30),
            Icon(Icons.perm_identity, size: 30),
          ],
          color: Colors.white,
          buttonBackgroundColor: Colors.white,
          backgroundColor: Colors.blueAccent,
          animationCurve: Curves.easeInOut,
          animationDuration: Duration(milliseconds: 600),
          onTap: (index) {
            setState(() {
              _page = index;
            });
          },
        ),
appBar: AppBar(
  centerTitle: true,
  title: Text("Online"),
  actions: <Widget>[
    IconButton(icon: Icon(Icons.search), onPressed: () {
showSearch(context: context, delegate: DataSearch());

    })
  ],
),     

body: Stack(
  
  
  children: <Widget>[
    
    
    SingleChildScrollView(
      child: Column(
        
        children: <Widget>
        [
_botonesRedondeados(),


        ],

      ),
    ),

  ],



    )


     );
   }
 Widget _botonesRedondeados(){

  return Table(children: [
    TableRow(
      children: [
       _crearBotonRedondeado(),
      
      ]
    ),
     TableRow(
      children: [
       _crearBotonRedondeado(),
       
      ]
    ),
     TableRow(
      children: [
       _crearBotonRedondeado(),
     
      ]
    ),
     TableRow(
      children: [
       _crearBotonRedondeado(),
      
      ]
    ),
   
  ],);
}



//Este widget se crea con el fin de tener cards perzonalizadas
  /*Widget _crearBotonRedondeado(){
    return Container(
height: 190.0,
margin: EdgeInsets.all(15.0),
decoration: BoxDecoration(
  color: Color.fromRGBO(62, 66, 107, 07),
  borderRadius:  BorderRadius.circular(20.0)
),
child: Column(
  mainAxisAlignment: MainAxisAlignment.spaceAround,
  children: <Widget>[
    
  FadeInImage(image: AssetImage('assets/calcu.jpg') ,
  width: 150.0,
  height: 150.0,
placeholder: AssetImage('assets/carga.gif')),

    Text('cosa', style:  TextStyle( color: Colors.pinkAccent))

  ]
),
    );

}*/

Widget _crearBotonRedondeado()
{
  return
     Card(
    
    margin: EdgeInsets.all(15.0),
     color: Color.fromRGBO(62, 66, 107, 07),
 
    
    
    
    child: Column(
      
      
      
      children: <Widget>[
        FadeInImage(image: AssetImage('assets/calcu.jpg') ,
  width: 200.0,
  height: 200.0,
placeholder: AssetImage('assets/carga.gif'),
fadeInDuration: Duration(milliseconds: 200),
),


Container(
  padding: EdgeInsets.all(10.0),
  child: Text('HOLA MUNDO'),
)


      ]
    ),
  
  );
}
   @override
   void debugFillProperties(DiagnosticPropertiesBuilder properties) {
     super.debugFillProperties(properties);
     properties.add(IntProperty('_page', _page));
   }


}//es del final de la clase despues del scafol





class DataSearch extends SearchDelegate<String>{

final cities =[
"matematicas",
"español",
"Fisica",
"ingles",
"Quimica",
"Programacion"


];

final recentCities = [
  "Fisica",
"ingles",
"Quimica",
"Programacion",

];



@override
List<Widget> buildActions(BuildContext context)
{
  return [IconButton(icon: Icon(Icons.clear), onPressed:() {
    query = "";
  })];
}

@override
Widget buildLeading(BuildContext context)
{
 return IconButton(icon: AnimatedIcon(
   icon: AnimatedIcons.menu_arrow,
   progress: transitionAnimation,
 ),
 
  onPressed: () {
    close(context, null);
  });
}


@override
Widget buildResults(BuildContext context)
{
  return Center(child:Container(
    height: 100.0,
    width: 100.0,

    child: Card(
color: Colors.red,
shape: StadiumBorder(),
child: Center(
  child: Text(query),
),
  ),
  )
  );
}


@override
Widget buildSuggestions(BuildContext context)
{
  //Con estas lineas de codigo es posible que el buscador funcione y ademas convirtiendo letras mayusculas y minusculas para que busque apesar de que se escriba diferente
  final suggestionList = query.isEmpty ? recentCities : cities.where((p)=>p.toLowerCase().contains(query.toLowerCase())).toList();

  return ListView.builder(itemBuilder: (context,index)=>ListTile
  (
    onTap:(){
      showResults(context);
    } ,
    leading: Icon(Icons.book),
  title:  RichText(text: TextSpan(
    text: suggestionList[index].substring(0,query.length),
    style:  TextStyle(color: Colors.black, fontWeight: FontWeight.bold),
children: [TextSpan(
  text:  suggestionList[index].substring(query.length),
  style:  TextStyle(color: Colors.grey),

)]
   ) ),
  ),
  itemCount: suggestionList.length,
  );
}



}
