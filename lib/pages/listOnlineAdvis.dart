import 'dart:convert';

import 'package:advis/modal/advisOnline.dart';
import 'package:advis/pages/profilePublicTeacher.dart';
import 'package:advis/services/env.dart';
import 'package:flutter/material.dart';
import 'package:http/http.dart';

class ListOnlineAdvis extends StatefulWidget {
  String id_skill;
  String  skillname;

  ListOnlineAdvis({this.id_skill, this.skillname});

  @override
  _ListOnlineAdvisState createState() => _ListOnlineAdvisState();
}

class _ListOnlineAdvisState extends State<ListOnlineAdvis> {
  List<OnlineAdvis> _list = new List<OnlineAdvis>();

  getDatosSkill() async {
    var peticion =
        await get(server + '/api/view/onlineAdvis/' + widget.id_skill);
    setState(() {
      dynamic datosSkill = json.decode(peticion.body);
      for (Map skill in datosSkill) {
        print(skill);
        _list.add(OnlineAdvis(
            id: skill["id"],
            last_name: skill["last_name"],
            profile_image: skill["profile_image"],
            level_skill: skill["level_skill"],
            id_skill: skill["id_skill"],
             email: skill["email"],
            name: skill["name"]));
      }
    });
  }

  @override
  void initState() {
    getDatosSkill();

    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SingleChildScrollView(
        child: ListView.builder(
          shrinkWrap: true,
          itemBuilder: (context, index) {
            return ListTile(
              onTap: () {
                Navigator.push(
                    context,
                    MaterialPageRoute(
                        builder: (context) => ProfilePublicTeacher(
                              _list[index].id.toString(),
                              _list[index].typeUser.toString(),
                              _list[index].email.toString(),
                              widget.skillname
                            )));
              },
              leading: CircleAvatar(
                backgroundImage: NetworkImage(
                  server + _list[index].profile_image,
                ),
                radius: 40,
              ),
              title: Text(_list[index].name + " " + _list[index].last_name),
              subtitle: Text(_list[index].level_skill),
            );
          },
          itemCount: _list.length,
        ),
      ),
    );
  }
}
