import 'package:advis/helper/constants.dart';
import 'package:advis/helper/helperfunctions.dart';
import 'package:advis/pages/mapScreen.dart';
import 'package:advis/pages/search.dart';
import 'package:advis/pages/navigation.dart';
import 'package:advis/services/env.dart';
import 'package:flutter/material.dart';
import 'package:http/http.dart';
import 'dart:convert';
import 'package:date_field/date_field.dart';


class EditProfileTeacher extends StatefulWidget {

  final email;

  

    EditProfileTeacher(this.email);
  @override
  _EditProfileTeacherState createState() => _EditProfileTeacherState();
}

class _EditProfileTeacherState extends State<EditProfileTeacher> {
 

  String name = "";
  String lastName = "";
  String phone = "";
  String email = "";
  String password = "";
  bool typeUser = true;
  DateTime selectedDate;
  

@override
void initState() {
    email = widget.email;
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(title: Text('Nueva información')),
        body: Card(
          child: SingleChildScrollView(
              child: Column(
            children: <Widget>[
              Center(
                child: Container(
                    padding: EdgeInsets.all(10),
                    child: Text(
                      '',
                      style: TextStyle(fontSize: 20),
                    )),
              ),
              Container(
                padding: EdgeInsets.all(30),
                child: TextField(
                  onChanged: (texto) {
                    name = texto;
                  },
                  decoration: InputDecoration(
                      border: OutlineInputBorder(),
                      labelText: 'Nombre',

                      
                      hintText: userLogged.name),
                      
                ),
              ),
              Container(
                padding: EdgeInsets.all(30),
                child: TextField(
                  onChanged: (texto) {
                    lastName = texto;
                  },
                  decoration: InputDecoration(
                      border: OutlineInputBorder(),
                      labelText: 'Apellido',

                      hintText: userLogged.last_name),
                ),
              ),
              Container(
                padding: EdgeInsets.all(30),
                child: TextField(
                  onChanged: (texto) {
                    phone = texto;
                  },
                  decoration: InputDecoration(
                      border: OutlineInputBorder(),
                      labelText: 'Numero telefónico',

                      hintText: userLogged.phone_number),
                    
                ),
              ),
              /*Container(
                padding: EdgeInsets.all(30),
                child: TextField(
                  onChanged: (texto) {
                    email = texto;
                  },
                  decoration: InputDecoration(
                      border: OutlineInputBorder(),
                      labelText: 'Correo electronico',
                      hintText: 'Ingresa tu correo electronico activo'),
                ),
              ),
              Container(
                padding: EdgeInsets.all(30),
                child: TextField(
                  onChanged: (texto) {
                    password = texto;
                  },
                  obscureText: true,
                  decoration: InputDecoration(
                      border: OutlineInputBorder(),
                      labelText: 'ingresa una contraseña porfavor',
                      hintText: 'Contraseña'),
                ),
              ),*/
             
      
              FlatButton(
                onPressed: () {
                  //forgot password screen
                },
                textColor: Colors.blue,
                child: Text(''),
              ),
              Container(
                  height: 50,
                  padding: EdgeInsets.fromLTRB(10, 0, 10, 0),
                  child: RaisedButton(
                    textColor: Colors.white,
                    color: Colors.blue,
                    child: Text('Finalizar'),
                    onPressed: () {
                      storeNewUser();
                      Navigator.pop(context);
             
                    },
                  )),
              /*Container(
                  child: Row(
                children: <Widget>[
                  Text('¿No tiene cuenta?'),
                  FlatButton(
                    textColor: Colors.blue,
                    child: Text(
                      'Registrarse',
                      style: TextStyle(fontSize: 20),
                    ),
                    onPressed: () {
                      storeNewUser();
                      //signup screen
                    },
                  )
                ],
                mainAxisAlignment: MainAxisAlignment.center,
              ))*/
            ],
          )),
        ));
  }

  storeNewUser() async {
    Map body = {
      "name": name,
      "last_name": lastName,
      "phone_number": phone,
      "email": email,
   
    };

    String datosEnjson = json.encode(body);
print(datosEnjson);
  

Response response =
        await post(server + '/api/edit/profileTeacher', body:body );
    print(response.statusCode);

    if(response.statusCode == 200 )
    {
      HelperFunctions.getUserData(Constants.myEmail);

    }
  
     


  }
}
