//en esta vista se pretende que se muestre despues de que se incio seccion en el dispositivo
//se visualiza las materias que tienen disponibles asesores para las asesorias

import 'package:advis/helper/constants.dart';
import 'package:advis/pages/chatRoomsScreen.dart';
import 'package:advis/pages/listOnlineAdvis.dart';
import 'package:advis/pages/mapScreen.dart';
import 'package:advis/pages/notification.dart';
import 'package:advis/pages/online_consultancies.dart';
import 'package:advis/pages/perfil_user_estudent.dart';
import 'package:advis/pages/profilePrivateTeacher.dart';
import 'package:advis/pages/profile_user_teacher.dart';
import 'package:advis/services/env.dart';
import 'package:flutter/material.dart';
import 'package:curved_navigation_bar/curved_navigation_bar.dart';
import 'package:http/http.dart';
import 'dart:convert';

class ConsultanciesView extends StatefulWidget {
  @override
  _ConsultanciesViewState createState() => _ConsultanciesViewState();
}

class _ConsultanciesViewState extends State<ConsultanciesView> {
  var datosSkill = [];

  getDatosSkill() async {
    var peticion = await get(server + '/api/view/skill');
    setState(() {
      datosSkill = json.decode(peticion.body);
    });

    print(datosSkill);
  }

  @override
  void initState() {
    getDatosSkill();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Stack(
      children: <Widget>[
        GridView.builder(
            gridDelegate:
                SliverGridDelegateWithFixedCrossAxisCount(crossAxisCount: 2),
            itemCount: datosSkill.length,
            itemBuilder: (context, index) {
              return _crearBotonRedondeado(
                  server + datosSkill[index]['skill_image'],
                  datosSkill[index]['name'],context,datosSkill[index]['id'].toString() );

            }),
      ],
    );
  }
}

Widget _crearBotonRedondeado(String picture, String name, context,String id_skill) {
  return Card(
    margin: EdgeInsets.all(15.0),
    child: InkWell(
      onTap: (){
             Navigator.push(
              context, MaterialPageRoute(builder: (context) => ListOnlineAdvis(id_skill: id_skill, skillname: name)));

      },
          child: Column(
        children: [
          Image.network(
            picture,
            //   height: 150,
          ),
          Text(
            name,
            style: TextStyle(fontSize: 23),
          ),
        ],
      ),
    ),
  );

/* return Card(
      margin: EdgeInsets.all(15.0),
      color: Color.fromRGBO(62, 66, 107, 07),
      child: Container(
        height: 500,
        child: Column(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: <Widget>[
              Image.network(
                picture,
                height: 150,
              ),
              Container(
                padding: EdgeInsets.all(10.0),
                child: Text(name),
              )
            ]),
      ));*/
}

class DataSearch extends SearchDelegate<String> {
  final cities = [
    "matematicas",
    "español",
    "Fisica",
    "ingles",
    "Quimica",
    "Programacion"
  ];

  final recentCities = [
    "Fisica",
    "ingles",
    "Quimica",
    "Programacion",
  ];

  @override
  List<Widget> buildActions(BuildContext context) {
    return [
      IconButton(
          icon: Icon(Icons.clear),
          onPressed: () {
            query = "";
          })
    ];
  }

  @override
  Widget buildLeading(BuildContext context) {
    return IconButton(
        icon: AnimatedIcon(
          icon: AnimatedIcons.menu_arrow,
          progress: transitionAnimation,
        ),
        onPressed: () {
          close(context, null);
        });
  }

  @override
  Widget buildResults(BuildContext context) {
    return Center(
        child: Container(
      height: 100.0,
      width: 100.0,
      child: Card(
        color: Colors.red,
        shape: StadiumBorder(),
        child: Center(
          child: Text(query),
        ),
      ),
    ));
  }

  @override
  Widget buildSuggestions(BuildContext context) {
    //Con estas lineas de codigo es posible que el buscador funcione y ademas convirtiendo letras mayusculas y minusculas para que busque apesar de que se escriba diferente
    final suggestionList = query.isEmpty
        ? recentCities
        : cities
            .where((p) => p.toLowerCase().contains(query.toLowerCase()))
            .toList();

    return ListView.builder(
      itemBuilder: (context, index) => ListTile(
        onTap: () {
          showResults(context);
        },
        leading: Icon(Icons.book),
        title: RichText(
            text: TextSpan(
                text: suggestionList[index].substring(0, query.length),
                style:
                    TextStyle(color: Colors.black, fontWeight: FontWeight.bold),
                children: [
              TextSpan(
                text: suggestionList[index].substring(query.length),
                style: TextStyle(color: Colors.grey),
              )
            ])),
      ),
      itemCount: suggestionList.length,
    );
  }
}
