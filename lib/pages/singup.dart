import 'package:advis/helper/helperfunctions.dart';
import 'package:advis/pages/chatRoomsScreen.dart';
import 'package:advis/pages/registerUserInformation.dart';
import 'package:advis/services/auth.dart';
import 'package:advis/services/database.dart';
import 'package:flutter/material.dart';
import 'package:advis/Widgets/widget.dart';

class SignUp extends StatefulWidget {
  final Function toogle;
  SignUp(this.toogle);
  @override
  _SignUpState createState() => _SignUpState();
}

class _SignUpState extends State<SignUp> {
  bool isLoading = false;
  AuthService authService = new AuthService();
  DatabaseMethods databaseMethods = new DatabaseMethods();
  HelperFunctions helperFunctions = new HelperFunctions();

  final formKey = GlobalKey<FormState>();
  TextEditingController userNameTextEdittingController =
      new TextEditingController();
  TextEditingController emailNameTextEdittingController =
      new TextEditingController();
  TextEditingController passwordNameTextEdittingController =
      new TextEditingController();

  sigMeUp() {
    if (formKey.currentState.validate()) {
      Map<String,dynamic> userInfoMap = {
        "name": userNameTextEdittingController.text,
        "email": emailNameTextEdittingController.text,
        "latitude":31.7461083,
        "longitude":-106.43119,
        "active":false
      };
 
      HelperFunctions.saveUserNameEmailSharedPreference(
          emailNameTextEdittingController.text);
      HelperFunctions.saveUserNameSharedPreference(
          userNameTextEdittingController.text);

      setState(() {
        isLoading = true;
      });

      authService
          .signUpWithEmailAndPassword(emailNameTextEdittingController.text,
              passwordNameTextEdittingController.text)
          .then((val) {
        //print("${val.udI}");
        //en esta parte tengo que cambiar ala vista dependiendo del tipo de usuario

        databaseMethods.uploadUserInfo(userInfoMap);
        HelperFunctions.saveuserLoggedInSharedPreference(true);
        Navigator.pushReplacement(
            context,
            MaterialPageRoute(
                builder: (context) =>
                    RegisterUser(emailNameTextEdittingController.text)));
      });
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: appBarMain(context),
      body: isLoading
          ? Container(child: Center(child: CircularProgressIndicator()))
          : SingleChildScrollView(
              padding: EdgeInsets.symmetric(horizontal: 24),
              child: Column(
                children: [
                  /*esta parte son los capos que son para usuario 
              y contraseña hedera los widgets del archivo widget.dart que pasa
              como parametros el nombre y seencarga de la decoracion*/
                  Column(
                    children: [
                      Form(
                          key: formKey,
                          child: Column(
                            children: [
                              TextFormField(
                                validator: (val) {
                                  return val.isEmpty || val.length < 2
                                      ? "Porfavor ingresar un usuario valido"
                                      : null;
                                },
                                controller: userNameTextEdittingController,
                                style: simpleTextStyle(),
                                decoration: textFieldInputDecoration(
                                    'Nombre de usuario'),
                              ),
                              TextFormField(
                                validator: (val) {
                                  return RegExp(
                                              r"^[a-zA-Z0-9.a-zA-Z0-9.!#$%&'*+-/=?^_`{|}~]+@[a-zA-Z0-9]+\.[a-zA-Z]+")
                                          .hasMatch(val)
                                      ? null
                                      : "Ingresar un correo valido";
                                },
                                controller: emailNameTextEdittingController,
                                style: simpleTextStyle(),
                                decoration: textFieldInputDecoration(
                                    'Correo electrónico'),
                              ),
                              TextFormField(
                                obscureText: true,
                                validator: (val) {
                                  return val.length > 6
                                      ? null
                                      : "Porfavor ingresar una contraseña valida arriba de 6 cacarteres";
                                },
                                controller: passwordNameTextEdittingController,
                                style: simpleTextStyle(),
                                decoration:
                                    textFieldInputDecoration('Contraseña'),
                              ),
                            ],
                          ))
                    ],
                  ),
                  /*Esta parte es para dar click si se olvido la contraseña*/
                  SizedBox(
                    height: 8,
                  ),
                  Container(
                    alignment: Alignment.centerRight,
                    child: Container(
                        padding:
                            EdgeInsets.symmetric(horizontal: 16, vertical: 8),
                        child: Text(
                          "¿Olvidaste tu contraseña?",
                          style: simpleTextStyle(),
                        )),
                  ),

//Aqui termina el diseño y funcionalidas del olvidaste contraseña
                  SizedBox(
                    height: 16,
                  ),
                  /*este contenedor se creo con el fin de crear el boton de iniciar sesion*/
                  GestureDetector(
                    onTap: () {
                      sigMeUp();
                    },
                    child: Container(
                      alignment: Alignment.center,
                      width: MediaQuery.of(context).size.width,
                      padding: EdgeInsets.symmetric(vertical: 20),
                      decoration: BoxDecoration(
                          gradient: LinearGradient(colors: [
                            const Color(0xff007EF4),
                            const Color(0xff2A75BC)
                          ]),
                          borderRadius: BorderRadius.circular(30)),
                      child: Text(
                        "Registrate",
                        style: mediumTextStyle(),
                      ),
                    ),
                  ), //eaaaa
                  /*Este contenedor fue creado con el proposito de poner el boton de iniciar session con google*/
                  SizedBox(
                    height: 16,
                  ),
                  Container(
                    alignment: Alignment.center,
                    width: MediaQuery.of(context).size.width,
                    padding: EdgeInsets.symmetric(vertical: 20),
                    decoration: BoxDecoration(
                        color: Colors.grey,
                        borderRadius: BorderRadius.circular(30)),
                    child: Text(
                      "Iniciar sesión con Google",
                      style: TextStyle(color: Colors.black87, fontSize: 17),
                    ),
                  ),

/*aqui se realizo la leyenda que indica que si no tienes contraseña te puedes registrar*/
                  SizedBox(
                    height: 16,
                  ),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      Text(
                        '¿Ya tienes una cuenta?',
                        style: mediumTextStyle(),
                      ),
                      GestureDetector(
                        onTap: () {
                          widget.toogle();
                        },
                        child: Container(
                          padding: EdgeInsets.symmetric(vertical: 8),
                          child: Text('Iniciar ahora',
                              style: TextStyle(
                                  color: Colors.black87,
                                  fontSize: 17,
                                  decoration: TextDecoration.underline)),
                        ),
                      ),
                    ],
                  ),
                ],
              ),
            ),
    );
  }
}
